﻿using CreditExchange.Syndication.Perfios;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Perfios.Persistance
{
    public class PerfiosRepository : MongoRepository<IPerfiosData, PerfiosData>, IPerfiosRepository
    {
        static PerfiosRepository()
        {
            BsonClassMap.RegisterClassMap<PerfiosData>(map =>
            {
                map.AutoMap();
                var type = typeof(PerfiosData);
                map.MapMember(m => m.ProcessDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public PerfiosRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "perfios-data")
        {
            CreateIndexIfNotExists("perfios-data",
                Builders<IPerfiosData>.IndexKeys.Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }


        public async Task<IPerfiosData> GetByEntityId(string entityType, string entityId)
        {
            return await Query.FirstOrDefaultAsync(a => a.EntityType == entityType && a.EntityId == entityId);
        }


    }
}
