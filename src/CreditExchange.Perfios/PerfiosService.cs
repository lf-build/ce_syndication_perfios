﻿
using CreditExchange.Syndication.Perfios;
using CreditExchange.Perfios.Events;
using CreditExchange.Syndication.Perfios.Request;
using CreditExchange.Syndication.Perfios.Response;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using System;
using System.Threading.Tasks;
using System.Linq;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace CreditExchange.Perfios
{
    public class PerfiosService : IPerfiosService
    {
        public PerfiosService(IPerfiosSyndicationService perfiosSyndicationService, IPerfiosRepository perfiosRepository,
            IEventHubClient eventHub, ILookupService lookup, IPerfiosConfiguration PerfiosConfiguration)
        {
            if (perfiosSyndicationService == null)
                throw new ArgumentNullException(nameof(perfiosSyndicationService));
            PerfiosSyndicationService = perfiosSyndicationService;
            if (perfiosRepository == null)
                throw new ArgumentNullException(nameof(perfiosRepository));
            PerfiosRepository = perfiosRepository;
            EventHub = eventHub;
            Lookup = lookup;

        }

        private IPerfiosSyndicationService PerfiosSyndicationService { get; }
        private IPerfiosRepository PerfiosRepository { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        public static string ServiceName { get; } = "perfios";

        public async Task<string> GenerateNetLinkingForm(string entityType, string entityId, IStartProcessRequest startProcessRequest)
        {
            entityType = EnsureEntityType(entityType);
            if (startProcessRequest == null)
                throw new ArgumentNullException(nameof(startProcessRequest));
            var referencenumber = Guid.NewGuid().ToString("N");
            startProcessRequest.TransactionId = referencenumber;
            var response = await PerfiosSyndicationService.StartProcessForm(startProcessRequest);
            var perfiosTransactionId = await PerfiosRepository.GetByEntityId(entityType, entityId);
            var perfiosData = new PerfiosData() { EntityType = entityType, EntityId = entityId, TransactionId = referencenumber, Referencenumber = referencenumber };
            if (perfiosTransactionId == null)
            {

                PerfiosRepository.Add(perfiosData);
            }
            else
            {
                perfiosTransactionId.TransactionId = referencenumber;
                perfiosTransactionId.Referencenumber = referencenumber;
                PerfiosRepository.Update(perfiosTransactionId);
            }
            await EventHub.Publish(new PerfiosNetLinkingFormRequested()
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = response,
                Request = startProcessRequest,
                ReferenceNumber = referencenumber,
                Name = ServiceName
            });

            return response;
        }
        public async Task<IStartTransactionResponse> StartTransaction(string entityType, string entityId, IStartTransactionRequest startTransactionRequest)
        {
            entityType = EnsureEntityType(entityType);
            if (startTransactionRequest == null)
                throw new ArgumentNullException(nameof(startTransactionRequest));

            var response = await PerfiosSyndicationService.StartTransaction(startTransactionRequest);
            var perfiosTransactionId = await PerfiosRepository.GetByEntityId(entityType, entityId);
            var perfiosData = new PerfiosData() { EntityType = entityType, EntityId = entityId, PerfiosTransactionId = response.PerfiosTransactionId, Referencenumber = response.Referencenumber };
            if (perfiosTransactionId == null)
                PerfiosRepository.Add(perfiosData);
            else
            {
                perfiosTransactionId.PerfiosTransactionId = response.PerfiosTransactionId;
                perfiosTransactionId.Referencenumber = response.Referencenumber;
                PerfiosRepository.Update(perfiosTransactionId);
            }
            await EventHub.Publish(new PerfiosTransactionStarted()
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = response,
                Request = startTransactionRequest,
                ReferenceNumber = response.Referencenumber,
                Name = ServiceName
            });

            return response;
        }

        public async Task<ITransactionCompleted> CompleteTranscation(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityId));

            var perfiosTransaction = await PerfiosRepository.GetByEntityId(entityType, entityId);
            if (perfiosTransaction == null)
            {
                throw new NotFoundException("Transaction is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }

            var CompleteTransactionresponse = await PerfiosSyndicationService.CompleteTranscation(perfiosTransaction.PerfiosTransactionId);
            var referencenumber = Guid.NewGuid().ToString("N");
            var response = new PerfiosTransactionCompleted()
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = CompleteTransactionresponse.Message,
                Request = new { perfiosTransaction.PerfiosTransactionId },
                ReferenceNumber = referencenumber,
                Name = ServiceName
            };

            await EventHub.Publish(response);
            return response;
        }
        public async Task<IAnalysisReport> RetrieveXmlReport(string entityType, string entityId)
        {

            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityId));
            var referencenumber = Guid.NewGuid().ToString("N");
            var perfiosTransaction = await PerfiosRepository.GetByEntityId(entityType, entityId);
            if (perfiosTransaction == null)
            {
                throw new NotFoundException("Transaction is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            try
            {
                var xmlReport = await PerfiosSyndicationService.RetrieveXmlReport(perfiosTransaction.PerfiosTransactionId, perfiosTransaction.Referencenumber);
                if (xmlReport != null)
                {
                    perfiosTransaction.XMLResponse = xmlReport.XMLResponseStr;
                    perfiosTransaction.XMLRequest = xmlReport.XMLRequestStr;
                    perfiosTransaction.ProcessDate = DateTime.Now;
                    if (xmlReport?.AnalysisReport != null)

                        perfiosTransaction.Status = "success";

                    else

                        perfiosTransaction.Status = "failed";

                    PerfiosRepository.Update(perfiosTransaction);
                    if (xmlReport?.AnalysisReport != null)
                    {
                        xmlReport.AnalysisReport.ReferenceNumber = referencenumber;
                        xmlReport.AnalysisReport.StatementPassword = perfiosTransaction.Password;

                        await EventHub.Publish(new PerfiosXmlReportPulled()
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = xmlReport?.AnalysisReport,
                            Request = new { perfiosTransaction.PerfiosTransactionId, perfiosTransaction.Referencenumber },
                            ReferenceNumber = referencenumber,
                            Name = ServiceName
                        });
                    }
                    else
                    {
                        throw new Exception(xmlReport.XMLResponseStr);
                    }
                }

                return xmlReport?.AnalysisReport;
            }
            catch (Exception ex)
            {
                await EventHub.Publish(new PerfiosXmlReportFailed()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = new { perfiosTransaction.PerfiosTransactionId, perfiosTransaction.Referencenumber },
                    ReferenceNumber = referencenumber,
                    Name = ServiceName
                });
                //ITransactionStatusResponse response =await TransactionStatus(entityType, entityId);


                throw new PerfiosException(ex.Message);
            }
        }
        public async Task<IPdfReportResponse> RetrievePdfReport(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityId));

            var perfiosTransaction = await PerfiosRepository.GetByEntityId(entityType, entityId);
            if (perfiosTransaction == null)
            {
                throw new NotFoundException("Transaction is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            try
            {
                var response = await PerfiosSyndicationService.RetrievePdfReport(perfiosTransaction.PerfiosTransactionId, perfiosTransaction.Referencenumber);

                await EventHub.Publish(new PerfiosPdfReportPulled()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response.ReferenceNumber,
                    Request = new { perfiosTransaction.PerfiosTransactionId, perfiosTransaction.Referencenumber },
                    ReferenceNumber = response.ReferenceNumber,
                    Name = ServiceName
                });
                return response;
            }
            catch (Exception ex)
            {
                await EventHub.Publish(new PerfiosPdfReportPullFailed()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = new { perfiosTransaction.PerfiosTransactionId, perfiosTransaction.Referencenumber },
                    ReferenceNumber = "",
                    Name = ServiceName
                });
                //ITransactionStatusResponse response =await TransactionStatus(entityType, entityId);


                throw new PerfiosException(ex.Message);
            }
        }
        public async Task<string> TransactionStatus(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityId));


            var perfiosTransaction = await PerfiosRepository.GetByEntityId(entityType, entityId);
            if (perfiosTransaction == null)
            {
                throw new NotFoundException("Transaction is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }

            var response = await PerfiosSyndicationService.TransactionStatus(perfiosTransaction.Referencenumber);
            var perfiostransactionid = response.Part?.LastOrDefault()?.PerfiosTransactionId;

            perfiosTransaction.PerfiosTransactionId = perfiostransactionid;
            PerfiosRepository.Update(perfiosTransaction);

            var referencenumber = Guid.NewGuid().ToString("N");
            response.ReferenceNumber = referencenumber;
            if (response.Part?.LastOrDefault()?.Status == "success")
            {
                await EventHub.Publish(new PerfiosTransactionCompleted()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = new { perfiosTransaction.PerfiosTransactionId, perfiosTransaction.Referencenumber },
                    ReferenceNumber = referencenumber,
                    Name = ServiceName
                });
            }

            return response.Part?.LastOrDefault()?.Status;
        }
        public async Task<IUploadStatementResponse> UploadStatement(string entityType, string entityId, byte[] fileBytes, string password)
        {
            entityType = EnsureEntityType(entityType);
            if (fileBytes == null)
                throw new ArgumentNullException(nameof(fileBytes));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityId));

            var perfiosTransaction = await PerfiosRepository.GetByEntityId(entityType, entityId);
            if (perfiosTransaction == null)
            {
                throw new NotFoundException("Transaction is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            perfiosTransaction.File = fileBytes;
            perfiosTransaction.Password = password;
            var response = await PerfiosSyndicationService.UploadStatement(perfiosTransaction.PerfiosTransactionId, fileBytes, password);
            var referencenumber = Guid.NewGuid().ToString("N");
            await EventHub.Publish(new PerfiosStatementUploaded()
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = response,
                Request = "file upload",
                ReferenceNumber = referencenumber,
                Name = ServiceName

            });
            response.ReferenceNumber = referencenumber;
            PerfiosRepository.Update(perfiosTransaction);
            return response;
        }

        public async Task<ISupportedInstition> SupportedInstition()
        {
            var response = await PerfiosSyndicationService.SupportedInstition();
            var referencenumber = Guid.NewGuid().ToString("N");

            await EventHub.Publish(new PerfiosSupportedInstitutionListPulled()
            {
                Response = response,
                Request = "Supported Institions",
                ReferenceNumber = referencenumber,
                Name = ServiceName

            });
            response.ReferenceNumber = referencenumber;
            return response;
        }
        public async Task<IReviewTransactionResponse> ReviewTransaction(string entityType, string entityId)
        {
            var perfiosTransaction = await PerfiosRepository.GetByEntityId(entityType, entityId);
            if (perfiosTransaction == null)
            {
                throw new NotFoundException("Transaction is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }

            var reviewTransactionRequest = new ReviewTransactionRequest
            {
                EntityId = entityId,
                EntityType = entityType,
                PerfiosTransactionId = perfiosTransaction.PerfiosTransactionId
            };
            reviewTransactionRequest.EntityType = EnsureEntityType(reviewTransactionRequest.EntityType);
            if (reviewTransactionRequest == null)
                throw new ArgumentNullException(nameof(reviewTransactionRequest));

            var response = await PerfiosSyndicationService.ReviewTransaction(reviewTransactionRequest);
            await EventHub.Publish(new PerfiosTransactionReviewRequested()
            {
                EntityId = reviewTransactionRequest.EntityId,
                EntityType = reviewTransactionRequest.EntityType,
                Response = response,
                Request = reviewTransactionRequest,
                ReferenceNumber = response.ReferenceNumber,
                Name = ServiceName

            });
            return response;
        }
        public async Task<bool> GetForm26ASInfo(string entityType, string entityId)
        {
            var perfiosTransaction = await PerfiosRepository.GetByEntityId(entityType, entityId);
            if (perfiosTransaction == null)
            {
                throw new NotFoundException("Transaction is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                var xmlReport = await PerfiosSyndicationService.RetrieveXmlReport(perfiosTransaction.PerfiosTransactionId, perfiosTransaction.Referencenumber);
                if (xmlReport?.AnalysisReport?.Form26ASInfo != null)
                {
                    perfiosTransaction.IsForm26ASInfoAvailable = true;
                    PerfiosRepository.Update(perfiosTransaction);
                    return true;
                }
                await EventHub.Publish(new Form26ASInfoRequested()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = perfiosTransaction.IsForm26ASInfoAvailable,
                    Request = entityId,
                    ReferenceNumber = referencenumber,
                    Name = ServiceName
                });
            }
            catch (Exception ex)
            {
                return false;
                throw new PerfiosException(ex.Message);
            }

            return false;
        }
        public async Task<IDeletedataResponse> Deletedata(string entityType, string entityId)
        {
            var perfiosTransaction = await PerfiosRepository.GetByEntityId(entityType, entityId);
            if (perfiosTransaction == null)
            {
                throw new NotFoundException("Transaction is not started for EntityType :" + entityType + " and EntityId :" + entityId);
            }

            var deletedataRequest = new DeletedataRequest
            {
                EntityId = entityId,
                EntityType = entityType,
                PerfiosTransactionId = perfiosTransaction.PerfiosTransactionId
            };

            deletedataRequest.EntityType = EnsureEntityType(deletedataRequest.EntityType);
            if (deletedataRequest == null)
                throw new ArgumentNullException(nameof(deletedataRequest));

            var response = await PerfiosSyndicationService.Deletedata(deletedataRequest);
            await EventHub.Publish(new PerfiosBankStatementDataDeleted()
            {
                EntityId = deletedataRequest.EntityId,
                EntityType = deletedataRequest.EntityType,
                Response = response,
                Request = deletedataRequest,
                ReferenceNumber = response.ReferenceNumber,
                Name = ServiceName
            });
            return response;
        }
        public async Task UpdateTransaction(string entityType, string entityId, IUpdateTransactionRequest transaction)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(entityId));
            if (string.IsNullOrWhiteSpace(transaction?.TransactionId))
                throw new ArgumentException("TransactionId is required");
            try
            {
                var perfiosdata = await PerfiosRepository.GetByEntityId(entityType, entityId);

                if (perfiosdata == null)
                {
                    var perfiostransaction = new PerfiosData()
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        File = transaction.File,
                        IsForm26ASInfoAvailable = transaction.IsForm26ASInfoAvailable,
                        PerfiosTransactionId = transaction.PerfiosTransactionId,
                        Password = transaction.Password,
                        Referencenumber = transaction.TransactionId,
                        TransactionId = transaction.TransactionId,
                        XMLResponse = transaction.XMLResponse
                    };
                    PerfiosRepository.Add(perfiostransaction);
                }
                else
                {
                    perfiosdata.File = transaction.File;
                    perfiosdata.IsForm26ASInfoAvailable = transaction.IsForm26ASInfoAvailable;
                    perfiosdata.PerfiosTransactionId = transaction.PerfiosTransactionId;
                    perfiosdata.Password = transaction.Password;
                    perfiosdata.Referencenumber = transaction.TransactionId;
                    perfiosdata.TransactionId = transaction.TransactionId;
                    perfiosdata.XMLResponse = transaction.XMLResponse;
                    PerfiosRepository.Update(perfiosdata);

                }
                await EventHub.Publish(new PerfiosTransactionUpdated()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = transaction.TransactionId,
                    Request = transaction,
                    ReferenceNumber = transaction.TransactionId,
                    Name = ServiceName
                });
            }
            catch (Exception ex)
            {

                throw new PerfiosException(ex.Message);
            }

        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

    }
}
