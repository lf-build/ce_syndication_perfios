﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.Perfios;
using CreditExchange.Syndication.Perfios.Request;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
#endif
using RestSharp.Extensions;
using CreditExchange.Perfios;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.Perfios.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IPerfiosService perfiosDataservice,ILogger logger) : base(logger)
        {                     
            if (perfiosDataservice == null)
                throw new ArgumentNullException(nameof(perfiosDataservice));
            PerfiosDataservice = perfiosDataservice;
        }

        private IPerfiosService PerfiosDataservice { get; }

        [HttpPost("{entitytype}/{entityid}/start-transaction")]
#if DOTNET2
        [ProducesResponseType(typeof(Syndication.Perfios.Response.IStartTransactionResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> StartTransaction(string entityType, string entityId,
            [FromBody] StartTransactionRequest startTransactionRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response =await PerfiosDataservice.StartTransaction(entityType, entityId, startTransactionRequest);
                    return Ok(response);
     
                }
                catch (PerfiosException exception)
                {
                   return ErrorResult.BadRequest(exception.Description);
                }
            });
        }
        [HttpPost("{entitytype}/{entityid}/start-process")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetNetLinkingForm(string entityType, string entityId,
          [FromBody] StartProcessRequest startProcessRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await PerfiosDataservice.GenerateNetLinkingForm(entityType, entityId, startProcessRequest);
                    return Ok(response);

                }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }
        [HttpPost("{entitytype}/{entityid}/complete-transaction")]
#if DOTNET2
        [ProducesResponseType(typeof(Events.ITransactionCompleted), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> CompleteTranscation(string entityType, string entityId
           )
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await PerfiosDataservice.CompleteTranscation(entityType, entityId);
                        return Ok(response);                   
                 }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }

        [HttpGet("{entitytype}/{entityid}/xml")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Perfios.Response.IReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> RetrieveXmlReport(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {                    
                        return Ok(await PerfiosDataservice.RetrieveXmlReport(entityType, entityId));                   
                }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }

        [HttpGet("{entitytype}/{entityid}/pdf")]
#if DOTNET2
        [ProducesResponseType(typeof(CreditExchange.Syndication.Perfios.Response.IPdfReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> RetrievePdfReport(string entityType, string entityId
          )
        {
            return await ExecuteAsync(async () =>
            {
                try
                {                   
                        return Ok(await PerfiosDataservice.RetrievePdfReport(entityType, entityId));                   
                }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }

        [HttpGet("{entitytype}/{entityid}/status")]
#if DOTNET2
        [ProducesResponseType(typeof(String), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> TransactionStatus(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {                   
                        return Ok(await PerfiosDataservice.TransactionStatus(entityType, entityId));                 
                }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }

        //TODO : From here review pending

        [HttpPost("{entitytype}/{entityid}/upload-statement")]
#if DOTNET2
        [ProducesResponseType(typeof(Syndication.Perfios.Response.IUploadStatementResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UploadStatement(string entitytype, string entityid, string password,
            IFormFile file)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    if (file == null || file.Length <= 0)
                        throw new ArgumentException("Value cannot be null or whitespace.", nameof(file));
                   
                        var response =
                       await
                           PerfiosDataservice.UploadStatement(entitytype, entityid,
                               file.OpenReadStream().ReadAsBytes(),password);
                        return Ok(response);
                   
                }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }

        [HttpGet("supported-instition")]
#if DOTNET2
        [ProducesResponseType(typeof(Syndication.Perfios.Response.ISupportedInstition), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> SupportedInstition()
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await PerfiosDataservice.SupportedInstition();                
                    return Ok(response);
                }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });

        }

        [HttpPost("{entitytype}/{entityid}/review-transaction")]
#if DOTNET2
        [ProducesResponseType(typeof(Syndication.Perfios.Response.IReviewTransactionResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> ReviewTransaction(string entitytype, string entityid)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                        var response = await PerfiosDataservice.ReviewTransaction(entitytype, entityid);
                        return Ok(response);
                   
                }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest( exception.Description);
                }
            });
        }

        [HttpGet("{entitytype}/{entityid}/form26as-status")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetForm26ASInfo(string entitytype, string entityid)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var response = await PerfiosDataservice.GetForm26ASInfo(entitytype, entityid);
                    return Ok(response);

                }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }
        [HttpPut("{entitytype}/{entityid}/updatetransaction")]
#if DOTNET2
         [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdateTransaction(string entitytype, string entityid,[FromBody]UpdateTransactionRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    await PerfiosDataservice.UpdateTransaction(entitytype, entityid,request);
                    return new NoContentResult();
                }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
               
            });
        }
        [HttpDelete("{entitytype}/{entityid}/delete-data")]
#if DOTNET2
        [ProducesResponseType(typeof(Syndication.Perfios.Response.IDeletedataResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> Delete(string entitytype, string entityid)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {                     
                        var response = await PerfiosDataservice.Deletedata(entitytype, entityid);
                        return Ok(response);                  
                }
                catch (PerfiosException exception)
                {
                    return ErrorResult.BadRequest(exception.Description);
                }
            });
        }
    }
}
