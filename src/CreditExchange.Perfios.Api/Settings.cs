﻿using System;
using LendFoundry.Foundation.Services.Settings;

namespace CreditExchange.Perfios.Api {
    public class Settings {
        public static string ServiceName { get; } = "perfios";

        private static string Prefix { get; } = ServiceName.ToUpper ();

        public static ServiceSettings EventHub { get; } = new ServiceSettings ($"{Prefix}_EVENTHUB", "eventhub");
        public static string Nats => Environment.GetEnvironmentVariable ($"{Prefix}_NATS_URL") ?? "nats";
        public static ServiceSettings Tenant { get; } = new ServiceSettings ($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings Configuration { get; } = new ServiceSettings ($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings TlsProxy { get; } = new ServiceSettings ($"{Prefix}_TLSPROXY", "tlsproxy");
        public static ServiceSettings LookupService { get; } = new ServiceSettings ($"{Prefix}_LOOKUPSERVICE", "lookup-service");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings ($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "perfios-Data");
    }
}