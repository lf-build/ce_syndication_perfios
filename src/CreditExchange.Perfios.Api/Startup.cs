﻿using CreditExchange.Syndication.Perfios;
using CreditExchange.Syndication.Perfios.Proxy;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
#else
using LendFoundry.Foundation.Documentation;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
#endif
using CreditExchange.Perfios.Persistance;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Persistence.Mongo;

namespace CreditExchange.Perfios.Api {
    public class Startup {
        public void ConfigureServices (IServiceCollection services) {
#if DOTNET2
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", new Info {
                    Version = "v1",
                        Title = "Perfios"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme () {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "CreditExchange.Perfios.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();

#else
            services.AddSwaggerDocumentation ();
#endif
            // services
            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddTenantTime ();

            // interface implements
            services.AddConfigurationService<PerfiosConfiguration> (Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub (Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService (Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddSingleton<IMongoConfiguration> (p => new MongoConfiguration (Settings.Mongo.ConnectionString, Settings.Mongo.Database));

            services.AddTransient<IPerfiosConfiguration> (p => {
                var configuration = p.GetService<IConfigurationService<PerfiosConfiguration>> ().Get ();
                // configuration.ProxyUrl = $"http://{Settings.TlsProxy.Host}:{Settings.TlsProxy.Port}";
                return configuration;
            });
            services.AddLookupService (Settings.LookupService.Host, Settings.LookupService.Port);

            services.AddTransient<IPerfiosProxy, PerfiosProxy> ();
            services.AddTransient<IPerfiosService, PerfiosService> ();
            services.AddTransient<IPerfiosRepository, PerfiosRepository> ();
            services.AddTransient<IPerfiosSyndicationService, PerfiosSyndicationService> ();
            // aspnet mvc related
            services.AddMvc ().AddLendFoundryJsonOptions ();
            services.AddCors ();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
#if DOTNET2
            app.UseSwagger ();

            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "MobileVerification Service");
            });
#else
            app.UseSwaggerDocumentation ();
#endif
            app.UseCors (env);
            app.UseErrorHandling ();
            app.UseRequestLogging ();
            app.UseMvc ();
            app.UseHealthCheck ();
        }

    }
}