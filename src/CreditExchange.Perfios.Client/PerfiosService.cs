﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using CreditExchange.Perfios.Events;
using CreditExchange.Syndication.Perfios.Request;
using CreditExchange.Syndication.Perfios.Response;
using RestSharp;
using System;

namespace CreditExchange.Perfios.Client
{
    public class PerfiosService :  IPerfiosService
    {
        public PerfiosService(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }

        public async Task<IStartTransactionResponse> StartTransaction(string entityType, string entityId, IStartTransactionRequest startTransactionRequest)
        {
             var request = new RestRequest("/{entitytype}/{entityid}/start-transaction", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(startTransactionRequest);
            return await Client.ExecuteAsync<StartTransactionResponse>(request);

        }
    
        public async Task<ITransactionCompleted> CompleteTranscation(string entityType, string entityId)
        {
            var request = new RestRequest("{entitytype}/{entityid}/complete-transaction", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);           
            return await Client.ExecuteAsync<PerfiosTransactionCompleted>(request);
        }

        public async Task<IAnalysisReport> RetrieveXmlReport(string entityType, string entityId)
        {

            var request = new RestRequest("{entitytype}/{entityid}/xml", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);    
            return await Client.ExecuteAsync<AnalysisReport>(request);
        }

        public async Task<IPdfReportResponse> RetrievePdfReport(string entityType, string entityId)
        {
            var request = new RestRequest("{entitytype}/{entityid}/pdf", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);            
            return await  Client.ExecuteAsync<PdfReportResponse>(request);
        }

        public async Task<string> TransactionStatus(string entityType, string entityId)
        {
            var request = new RestRequest("{entitytype}/{entityid}/status", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<string>(request);
        }


        public async Task<IUploadStatementResponse> UploadStatement(string entityType, string entityId, byte[] uploadStatementRequest,string password)
        {
            var request = new RestRequest("{entitytype}/{entityid}/upload-statement", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddFile("file", uploadStatementRequest, "file");
            request.AddParameter("password", password);
            return await Client.ExecuteAsync<UploadStatementResponse>(request);
        }
        public async Task<ISupportedInstition> SupportedInstition()
        {
            var request = new RestRequest("supported-instition", Method.GET);
            return await Client.ExecuteAsync<SupportedInstition>(request);
        }
        public async Task<IReviewTransactionResponse> ReviewTransaction(string entityType, string entityId)
        {
            var request = new RestRequest("{entitytype}/{entityid}/review-transaction", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<ReviewTransactionResponse>(request);
        }
        public async Task<IDeletedataResponse> Deletedata(string entityType, string entityId)
        {
            var request = new RestRequest("{entitytype}/{entityid}/delete-data", Method.DELETE);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<DeletedataResponse>(request);
        }

        public async Task<string> GenerateNetLinkingForm(string entityType, string entityId, IStartProcessRequest startProcessRequest)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/start-process", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(startProcessRequest);
            return await Client.ExecuteAsync<string>(request);
        }

     

        public async Task<bool> GetForm26ASInfo(string entityType, string entityId)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/form26as-status", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);

            return await Client.ExecuteAsync<bool>(request);
        }

        public Task UpdateTransaction(string entityType, string entityId, IUpdateTransactionRequest transaction)
        {
            throw new NotImplementedException();
        }
    }
}
