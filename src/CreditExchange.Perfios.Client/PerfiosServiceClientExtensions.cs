﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Perfios.Client
{
    public static class PerfiosServiceClientExtensions
    {
        public static IServiceCollection AddPerfiosService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IPerfiosServiceClientFactory>(p => new PerfiosServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IPerfiosServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
