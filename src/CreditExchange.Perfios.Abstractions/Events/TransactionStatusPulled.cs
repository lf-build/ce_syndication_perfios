﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Perfios.Events
{
    public class PerfiosTransactionStatusPulled : SyndicationCalledEvent
    {
    }
}
