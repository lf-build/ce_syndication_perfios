﻿using CreditExchange.Perfios.Events;
using CreditExchange.Syndication.Perfios.Request;
using CreditExchange.Syndication.Perfios.Response;
using System.Threading.Tasks;

namespace CreditExchange.Perfios
{
   public interface IPerfiosService
    {
        Task<IStartTransactionResponse> StartTransaction(string entityType, string entityId, IStartTransactionRequest startTransactionRequest);
        Task<ITransactionCompleted> CompleteTranscation(string entityType, string entityId);
        Task<IAnalysisReport> RetrieveXmlReport(string entityType, string entityId);
        Task<IPdfReportResponse> RetrievePdfReport(string entityType, string entityId);
        Task<string> TransactionStatus(string entityType, string entityId);
        Task<IUploadStatementResponse> UploadStatement(string entityType, string entityId, byte[] uploadStatementRequest,string password);
        Task<ISupportedInstition> SupportedInstition();
        Task<IReviewTransactionResponse> ReviewTransaction(string entitytype, string entityid);
        Task<IDeletedataResponse> Deletedata(string entitytype, string entityid);
        Task<string> GenerateNetLinkingForm(string entityType, string entityId, IStartProcessRequest startProcessRequest);
        Task<bool> GetForm26ASInfo(string entityType, string entityId);
        Task UpdateTransaction(string entityType, string entityId, IUpdateTransactionRequest transaction);
    }
}
