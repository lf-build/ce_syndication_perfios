﻿

using CreditExchange.Syndication.Perfios;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace CreditExchange.Perfios
{
    public interface IPerfiosRepository : IRepository<IPerfiosData>
    {
        Task<IPerfiosData> GetByEntityId(string entityType, string entityId);
    }
}
