﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Perfios.Request;
using CreditExchange.Syndication.Perfios.Response;
using CreditExchange.Syndication.Perfios.Proxy.Response;

namespace CreditExchange.Syndication.Perfios
{
    public interface IPerfiosSyndicationService
    {
        Task<IStartTransactionResponse> StartTransaction(IStartTransactionRequest startTransactionRequest);
        Task<CompleteTransactionResponse> CompleteTranscation(string perfiostransactionid);
        Task<IReportResponse> RetrieveXmlReport(string perfiosTransactionId, string transactionId);
        Task<IPdfReportResponse> RetrievePdfReport(string perfiosTransactionId, string transactionId);
        Task<ITransactionStatusResponse> TransactionStatus(string transactionId);
        Task<IUploadStatementResponse> UploadStatement(string perfiosTransactionId ,byte[] uploadStatementRequest,string password);
        Task<ISupportedInstition> SupportedInstition();
        Task<IReviewTransactionResponse> ReviewTransaction(IReviewTransactionRequest reviewTransactionRequest);
        Task<IDeletedataResponse> Deletedata(IDeletedataRequest deletedataRequest);
        Task<string> StartProcessForm(IStartProcessRequest startProcessRequest);
    }
}
