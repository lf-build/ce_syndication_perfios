﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios
{
    [XmlRoot(ElementName = "Error")]
    public class ErrorResponse : IErrorResponse
    {
        [XmlElement(ElementName = "code")]
        public string Code { get; set; }
        [XmlElement(ElementName = "message")]
        public string Message { get; set; }
        [XmlElement(ElementName = "statementErrorCode")]
        public string StatementErrorCode {get;set;}
    }
}
