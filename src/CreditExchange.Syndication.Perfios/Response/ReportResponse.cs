﻿using CreditExchange.Syndication.Perfios.Proxy.Response;

namespace CreditExchange.Syndication.Perfios.Response
{
    public class ReportResponse : IReportResponse
    {
        public ReportResponse()
        {

        }
        public ReportResponse(TransactionAnalysisReport response)
        {
            XMLResponseStr = response.ResponseXMLStr;
            XMLRequestStr = response.RequestXMLStr;
            if(response?.Report!=null)
            AnalysisReport = new AnalysisReport(response.Report);
        }
        public string XMLResponseStr { get; set; }
        public string XMLRequestStr { get; set; }
        public IAnalysisReport AnalysisReport { get; set; }
    }
}
