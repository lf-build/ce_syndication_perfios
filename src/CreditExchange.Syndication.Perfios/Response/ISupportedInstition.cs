﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Perfios.Response
{
    public interface ISupportedInstition
    {
        List<IInstitution> Instiutions { get; set; }
        string ReferenceNumber { get; set; }
    }
}