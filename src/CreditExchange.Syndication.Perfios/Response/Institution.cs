﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public class Institution : IInstitution
    {
        public Institution(Proxy.Response.Institution institution)
        {
            if(institution != null)
            {
                Id = institution.Id;
                Name = institution.Name;
                InstitutionType = institution.InstitutionType;
            }
        }
        public string Id { get; set; }
      
        public string Name { get; set; }
      
        public string InstitutionType { get; set; }
      
      
    }
}
