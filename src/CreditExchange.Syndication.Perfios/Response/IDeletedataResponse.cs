﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public interface IDeletedataResponse
    {
        string ReferenceNumber { get; set; }
        string Status { get; set; }
    }
}