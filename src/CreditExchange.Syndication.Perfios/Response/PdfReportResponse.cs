﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public class PdfReportResponse :IPdfReportResponse
    {
       public byte[] Report { get; set; }
        public  string ReferenceNumber { get; set; }
    }
}
