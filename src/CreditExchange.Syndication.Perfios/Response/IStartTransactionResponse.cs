﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public interface IStartTransactionResponse
    {
        string PerfiosTransactionId { get; set; }
        string Referencenumber { get; set; }
    }
}