﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public interface IUploadStatementResponse
    {
        string StatementId { get; set; }

        string ReferenceNumber { get; set; }

    }
}