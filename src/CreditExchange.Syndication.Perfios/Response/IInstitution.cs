﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public interface IInstitution
    {
        string Id { get; set; }
        string InstitutionType { get; set; }
        string Name { get; set; }
    }
}