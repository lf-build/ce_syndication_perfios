﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public class StartTransactionResponse : IStartTransactionResponse
    {
        public StartTransactionResponse(Proxy.Response.StartTransactionResponse startTransactionResponse)
        {
            if (startTransactionResponse != null)
                PerfiosTransactionId = startTransactionResponse.perfiosTransactionId;
        }
        public string PerfiosTransactionId { get; set; }

        public string TransactionId { get; set; }

        public string Referencenumber { get; set; }
    }
}
