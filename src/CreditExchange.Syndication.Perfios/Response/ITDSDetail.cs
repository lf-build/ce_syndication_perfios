﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Perfios.Response
{
    public interface ITDSDetail
    {
         string Fy { get; set; }
         string Ay { get; set; }
         List<ITDS> TDSs { get; set; }
    }
}
