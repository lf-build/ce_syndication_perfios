﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Perfios.Response
{
    public class TDSDetail :ITDSDetail
    {
        public TDSDetail()
        {

        }
        public TDSDetail(Proxy.Response.TDSDetails tdsDetail)
        {
            if (tdsDetail != null)
            {
                Fy = tdsDetail.fy;
                Ay = tdsDetail.ay;
                if (tdsDetail?.TDSs?.TDS != null)
                {
                    TDSs = tdsDetail?.TDSs?.TDS?.Select(i => new TDS(i)).ToList<ITDS>();
                }
            }
        }

        public string Fy { get; set; }
        public string Ay { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ITDS, TDS>))]
        public List<ITDS> TDSs { get; set; }
    }
}
