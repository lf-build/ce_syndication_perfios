﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public class MonthwiseBreakup :IMonthwiseBreakup
    {
        public MonthwiseBreakup(Proxy.Response.RecurringExpensesRecurringExpenseTotalPerMonth totalPerMonth)
        {
            if (totalPerMonth != null)
            {
                Month = totalPerMonth.month;
                Amount = totalPerMonth.amount.ToString();
            }
        }
        public string Month { get; set; }
        public string Amount { get; set; }
    }
}
