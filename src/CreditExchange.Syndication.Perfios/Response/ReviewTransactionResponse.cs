﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public class ReviewTransactionResponse :IReviewTransactionResponse
    {
        public string Status { get; set; }
        public  string ReferenceNumber { get; set; }
    }
}
