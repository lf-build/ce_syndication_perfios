﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Perfios.Response
{
    public interface ITransactionStatusResponse
    {
      
        string Files { get; set; }
        string Parts { get; set; }
        string Processing { get; set; }
       
        string ReferenceNumber { get; set; }
        List<IPart> Part { get; set; }
    }
    public interface IPart
    {

         string PerfiosTransactionId { get; set; }

         string Status { get; set; }

         string ErrorCode { get; set; }

         string Reason { get; set; }
    }
}