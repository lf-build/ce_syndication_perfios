﻿using System.Collections.Generic;
using CreditExchange.Syndication.Perfios.ReportInfo;
using CustomerInfo = CreditExchange.Syndication.Perfios.ReportInfo.CustomerInfo;
using RecurringExpense = CreditExchange.Syndication.Perfios.ReportInfo.RecurringExpense;
namespace CreditExchange.Syndication.Perfios.Response
{
    public interface IAnalysisReport
    {
        string AccountNo { get; set; }
        string AccountType { get; set; }
        string Bank { get; set; }
        ICustomerInfo CustomerInfo { get; set; }
        string DurationInMonths { get; set; }
        string EndDate { get; set; }
        IForm26ASInfo Form26ASInfo { get; set; }
        string IFSCCode { get; set; }
        string Location { get; set; }
        string MICRCode { get; set; }
        List<IRecurringExpense> RecurringExpenses { get; set; }
        string ReferenceNumber { get; set; }
        string StartDate { get; set; }
        string StatementPassword { get; set; }
        IStatementSummary StatementSummary { get; set; }
        List<ITranasactionInfo> Transactions { get; set; }
    }
}