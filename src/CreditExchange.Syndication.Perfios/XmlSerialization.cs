﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios
{
    internal sealed class ExtentedStringWriter : StringWriter
    {
        public ExtentedStringWriter(Encoding desiredEncoding)
        {
            Encoding = desiredEncoding;
        }

        public override Encoding Encoding { get; }
    }

    public static class XmlSerialization
    {
        public static string Serialize<T>(T @object)
        {
            string xmlContent;
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var sww = new ExtentedStringWriter(Encoding.UTF8))
            using (var writer = XmlWriter.Create(sww, new XmlWriterSettings() { NamespaceHandling = NamespaceHandling.OmitDuplicates, OmitXmlDeclaration = true }))
            {
                var namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);
                xmlSerializer.Serialize(writer, @object, namespaces);
                xmlContent = sww.ToString();
            }
            return xmlContent;
        }

        public static T Deserialize<T>(string xmlContent)
        {
            try
            {
                var xmlSerializer = new XmlSerializer(typeof(T));

                using (var sr = new StringReader(xmlContent))
                    return (T)xmlSerializer.Deserialize(sr);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}