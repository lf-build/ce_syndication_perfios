﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Perfios
{
    public class PerfiosConfiguration : IPerfiosConfiguration
    {
        public IServiceUrls ServiceUrls { get; set; } = new ServiceUrls();
        public string ApiVersion { get; set; } = "2.0";
        public string VendorId { get; set; } = "creditExchange";
        public string Destination { get; set; } = "statement";

        public string PrivateKey { get; set; } = "-----BEGIN RSA PRIVATE KEY-----\r\n" +
                                                 "MIIBOgIBAAJBAIk/YBItZo5OF3zb+lEjiMbrGGEKEThvvLb7L7R+pz6Ft3NVljHi\r\n" +
                                                 "Th0/rp1vpntjcbBYuD250ABBgYZXL8ZL3A8CAwEAAQJAXqjwSPk5P7MKrhpGlknM\r\n" +
                                                 "321sfhlkcSlX3lh2uaWVEiBC8EnrQbuUTFH/Z8DCPwg64jHrpj7ToC6fT9fyuL8S\r\n" +
                                                 "6QIhAMOFW2IzBmhzDIR/MT6QSyGk/ZS4lQCfWq+VYS3iK7eVAiEAs7OMAOaNQJgo\r\n" +
                                                 "3qKcwALSnfNH/EDjn3QCWontbIEZTBMCIFKKcujC57qijy9ETvK9kaozcAYf4m9v\r\n" +
                                                 "1qX3Zx4qtA/9AiAoLld5xBOFhABvd7DRBlCN3N4Vu3SqLMhx8jFSd7NuXQIhAK5h\r\n" +
                                                 "aMJVvxPr0TLgHGcIiDJw8bWOoA9ocvtXAOn5A6CE\r\n" +
                                                 "-----END RSA PRIVATE KEY-----";
        public string PublicKey { get; set; }
        public string AcceptancePolicy { get; set; }
        public string LoanType { get; set; } = "Personal";
        public string LoanDuration { get; set; } = "6";
        public string LoanAmount { get; set; } = "0";
        public string ProxyUrl { get; set; } = "";

        public string NetLinkForm { get; set; } = "<HTML><form id='perfiosnetlinkform' style='display:none' action='##StartNetLinkingProcessUrl##' method='POST'><input type='text' name='payload' value='##payload##' /><input type='text' name='signature' value='##signature##' /></form><script>document.getElementById('perfiosnetlinkform').submit();</script></HTML>";
        public bool UseProxy { get; set; } = true;
       // public List<string> RetryErrorCode { get; set; } = new List<string>(new string[] { "E_NO_ERROR" });
    }
}
