﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public interface IUpdateTransactionRequest
    {
        byte[] File { get; set; }
        bool IsForm26ASInfoAvailable { get; set; }
        string Password { get; set; }
        string PerfiosTransactionId { get; set; }
        string Referencenumber { get; set; }
        string TransactionId { get; set; }
        string XMLResponse { get; set; }
    }
}