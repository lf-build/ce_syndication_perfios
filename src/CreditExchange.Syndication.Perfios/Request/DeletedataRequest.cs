﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public class DeletedataRequest :Source, IDeletedataRequest
    {
        public string PerfiosTransactionId { get; set; }
    }
}
