﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public interface ITransactionStatusRequest :ISource
    {
        string TransactionId { get; set; }
    }
}