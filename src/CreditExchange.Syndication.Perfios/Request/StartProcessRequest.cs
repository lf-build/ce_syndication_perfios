﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public class StartProcessRequest :IStartProcessRequest
    {
        public  string EmailId { get; set; }
        public string YearMonthFrom { get; set; }
        public string YearMonthTo { get; set; }
        public string DOB { get; set; }
        public string ReturnUrl { get; set; }
        public string TransactionId { get; set; }
    }
}
