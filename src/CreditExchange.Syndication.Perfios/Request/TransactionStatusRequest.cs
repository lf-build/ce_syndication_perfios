﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public class TransactionStatusRequest :Source, ITransactionStatusRequest
    {
        public string TransactionId { get; set; }
    }
}
