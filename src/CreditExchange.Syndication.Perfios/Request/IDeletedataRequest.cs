﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public interface IDeletedataRequest :ISource
    {
        string PerfiosTransactionId { get; set; }
    }
}