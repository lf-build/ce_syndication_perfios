﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public interface IStartTransactionRequest
    {
        string InstitutionId { get; set; }
        string YearMonthFrom { get; set; }
        string YearMonthTo { get; set; }
    }
}