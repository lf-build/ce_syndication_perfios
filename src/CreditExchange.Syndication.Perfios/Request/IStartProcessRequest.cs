﻿namespace CreditExchange.Syndication.Perfios.Request
{
    public interface IStartProcessRequest
    {
        string EmailId { get; set; }
        string YearMonthFrom { get; set; }
        string YearMonthTo { get; set; }
        string DOB { get; set; }
        string ReturnUrl { get; set; }

        string TransactionId { get; set; }
    }
}
