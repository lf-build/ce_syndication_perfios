﻿namespace CreditExchange.Syndication.Perfios
{
    public class PerfiosInitializationRequest : IPerfiosInitializationRequest
    {
        public string InstitutionId { get; set; }
        public string YearMonthFrom { get; set; }
        public string YearMonthTo { get; set; }
        public byte[] fileBytes { get; set; }
        public string password { get; set; }
    }
}
