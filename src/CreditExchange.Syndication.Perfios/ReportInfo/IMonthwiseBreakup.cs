﻿namespace CreditExchange.Syndication.Perfios.Response
{
    public interface IMonthwiseBreakup
    {
        string Amount { get; set; }
        string Month { get; set; }
    }
}