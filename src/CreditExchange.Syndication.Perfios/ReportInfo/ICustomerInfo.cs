﻿namespace CreditExchange.Syndication.Perfios.ReportInfo
{
    public interface ICustomerInfo
    {
    
        string Address { get; set; }
        string Email { get; set; }
        string Landline { get; set; }
        string Mobile { get; set; }
        string Name { get; set; }
        string Pan { get; set; }
        string SourceOfData { get; set; }

    }
}