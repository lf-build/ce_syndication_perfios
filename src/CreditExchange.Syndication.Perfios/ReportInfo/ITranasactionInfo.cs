﻿namespace CreditExchange.Syndication.Perfios.ReportInfo
{
    public interface ITranasactionInfo
    {
        string Amount { get; set; }
        string Balance { get; set; }
        string Category { get; set; }
        string ChqNo { get; set; }
        string Date { get; set; }
        string Narration { get; set; }
    }
}