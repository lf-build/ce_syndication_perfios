﻿namespace CreditExchange.Syndication.Perfios.ReportInfo
{
    public class TranasactionInfo : ITranasactionInfo
    {
        public TranasactionInfo()
        {

        }
        public TranasactionInfo(Proxy.Response.AccountXN response)
        {
            if(response!=null)
            {
                Date = response.date.ToString();
                ChqNo = response.chqNo;
                Narration = response.narration;
                Amount = response.amount.ToString();
                Category = response.category;
                Balance = response.balance.ToString();
            }
        }
        public TranasactionInfo(Proxy.Response.RecurringExpensesRecurringExpenseKTXn response)
        {
            if (response != null)
            {
                Date = response.date.ToString();
                ChqNo = response.chqNo;
                Narration = response.narration;
                Amount = response.amount.ToString();
               

            }
        }
        public string Date { get; set; }
        public string ChqNo { get; set; }
        public string Narration { get; set; }
        public string Amount { get; set; }
        public string Category { get; set; }
        public string Balance { get; set; }
    }
}
