﻿using CreditExchange.Syndication.Perfios.Response;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CreditExchange.Syndication.Perfios.ReportInfo
{
    public class Form26ASInfo :IForm26ASInfo
    {
        public Form26ASInfo()
        {

        }
        public Form26ASInfo(Proxy.Response.Form26ASInfo form26ASInfo)
        {
            if (form26ASInfo?.PersonalDetails != null)
            {
                Name = form26ASInfo?.PersonalDetails.name;
                Address = form26ASInfo?.PersonalDetails.address;
                Pan = form26ASInfo?.PersonalDetails.pan;
                Dob = form26ASInfo?.PersonalDetails.dob;
                Panstatus = form26ASInfo?.PersonalDetails.panStatus;
            }
            if (form26ASInfo?.TDSDetails != null)
            {
                TDSDetail = form26ASInfo?.TDSDetails?.Select(i => new TDSDetail(i)).ToList<ITDSDetail>();
            }
        }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Dob { get; set; }
        public string Pan { get; set; }
        public string Panstatus { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ITDSDetail, TDSDetail>))]
        public List<ITDSDetail> TDSDetail { get; set; }
    }
}
