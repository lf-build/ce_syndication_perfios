﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using CreditExchange.Syndication.Perfios.Proxy.Response;

namespace CreditExchange.Syndication.Perfios {
    [Serializable]
    public class PerfiosException : Exception {
        private static Dictionary<string, string> StatementErrorCodes { get; }

        public PerfiosException () { }
        static PerfiosException () {
            StatementErrorCodes = new Dictionary<string, string> (StringComparer.InvariantCultureIgnoreCase);
            StatementErrorCodes.Add ("E_AMOUNT_BALANCE_MISMATCH", "Calculated amounts/balances do not match actual values");
            StatementErrorCodes.Add ("E_DATE_RANGE", "No bank transactions in the expected date range");
            StatementErrorCodes.Add ("E_FILE_IO", "Cannot open uploaded file");
            StatementErrorCodes.Add ("E_FILE_NOT_STATEMENT", "Uploaded file is not a valid bank statement");
            StatementErrorCodes.Add ("E_FILE_NO_PASSWORD", "No password provided to open a password protected file");
            StatementErrorCodes.Add ("E_FILE_UNSUPPORTED_FORMAT", "Unsupported file format");
            StatementErrorCodes.Add ("E_FILE_WRONG_PASSWORD", "Wrong password provided to open a password protected file");
            StatementErrorCodes.Add ("E_MISSING_TRANSACTIONS", "One or more transactions are missing in the specified date range");
            StatementErrorCodes.Add ("E_MULTIPLE_ERRORS", "There were multiple errors");
            StatementErrorCodes.Add ("E_NOT_ACCEPTED", "Transaction did not meet the criteria set by acceptance policy");
            StatementErrorCodes.Add ("E_NO_ACCOUNT", "User has no bank accounts");
            StatementErrorCodes.Add ("E_NO_ERROR", "No errors");
            StatementErrorCodes.Add ("E_OTHER", "Some other error occurred");
            StatementErrorCodes.Add ("E_SITE", "Site error");
            StatementErrorCodes.Add ("E_STATEMENT_ERRONEOUS_DATA", "Statement contains erroneous data");
            StatementErrorCodes.Add ("E_STATEMENT_MULTIPLE_ACCOUNTS", "Statement contains data from more than one bank account");
            StatementErrorCodes.Add ("E_STATEMENT_NO_TRANSACTIONS", "Statement contains no transactions");
            StatementErrorCodes.Add ("E_STATEMENT_POOR_QUALITY", "Unsupported statement format");
            StatementErrorCodes.Add ("E_STATEMENT_WRONG_FORMAT", "Wrong statement format");
            StatementErrorCodes.Add ("E_STATEMENT_WRONG_INSTITUTION", "User uploaded statement for wrong institution");
            StatementErrorCodes.Add ("E_STATEMENT_UNSUPPORTED_INSTITUTION", "User uploaded statement for an institution not supported by Perfios");
            StatementErrorCodes.Add ("E_SYSTEM", "E_SYSTEM");
            StatementErrorCodes.Add ("E_USER_ACTION", "Error caused by User action");
            StatementErrorCodes.Add ("E_USER_CANCELLED", "Perfios transaction was cancelled by user");
            StatementErrorCodes.Add ("E_USER_MULTIPLE_ACCOUNTS", "User has uploaded statements from multiple accounts");
            StatementErrorCodes.Add ("E_USER_SESSION_EXPIRED", "Session expired without the user taking any further action");
            StatementErrorCodes.Add ("E_WRONG_CREDENTIALS", "User provided wrong user name or password for institution");
        }
        public PerfiosException (string message) : base (message) {
            try {
                if (!string.IsNullOrWhiteSpace (message)) {

                    var errorsresponse = XmlSerialization.Deserialize<ErrorResponse> (message);
                    Code = errorsresponse?.Code;
                    if (!string.IsNullOrWhiteSpace (errorsresponse?.StatementErrorCode)) {
                        string StatementErrorDescription;
                        if (StatementErrorCodes.TryGetValue (errorsresponse.StatementErrorCode, out StatementErrorDescription)) {
                            Description = StatementErrorCodes[errorsresponse.StatementErrorCode];

                        } else {
                            Description = errorsresponse.Message;
                        }
                        //Description = StatementErrorCode[errorsresponse.StatementErrorCode].;
                    } else {
                        Description = errorsresponse.Message;
                    }
                } else {
                    Description = "Bad Request";
                }

            } catch (System.Exception ex) {

                Description = message;
            }

        }

        public PerfiosException (string message, Exception innerException) : base (message, innerException) { }

        protected PerfiosException (SerializationInfo info, StreamingContext context) : base (info, context) { }
        public string Code { get; set; }
        public string Description { get; set; }
        public string StatementErrorCode { get; set; }
    }
}