﻿using System.Threading.Tasks;
using CreditExchange.Syndication.Perfios.Proxy.Request;
using CreditExchange.Syndication.Perfios.Proxy.Response;
using DeletedataResponse = CreditExchange.Syndication.Perfios.Proxy.Response.DeletedataResponse;
using ReviewTransactionResponse = CreditExchange.Syndication.Perfios.Proxy.Response.ReviewTransactionResponse;
using StartTransactionResponse = CreditExchange.Syndication.Perfios.Proxy.Response.StartTransactionResponse;
using TransactionStatusResponse = CreditExchange.Syndication.Perfios.Proxy.Response.TransactionStatusResponse;
using UploadStatementResponse = CreditExchange.Syndication.Perfios.Proxy.Response.UploadStatementResponse;

namespace CreditExchange.Syndication.Perfios.Proxy
{
    public interface IPerfiosProxy
    {
       Task<CompleteTransactionResponse> CompleteTransaction(CompleteTransactionRequest request);
       Task<DeletedataResponse> Deletedata(DeletedataRequest request);
       Task<SupportedInstitutions> ListsupportedInstitutions(ListSupportedInstitutionRequest request);
        Task<TransactionAnalysisReport> RetrieveReportXml(RetrieveReportRequest request);
       Task<ReviewTransactionResponse> ReviewTransaction(ReviewTransactionRequest request);

       Task<byte[]> RetrieveReportPdf(RetrieveReportRequest request);
       Task<StartTransactionResponse> StartTransaction(StartTransactionRequest request);
       Task<TransactionStatusResponse> TransactionStatus(TransactionStatusRequest request);
      Task<UploadStatementResponse> UploadStatement(UploadStatementRequest request);
        Task<string> GenerateNetlinkingForm(StartProcessRequest request);
    }
}