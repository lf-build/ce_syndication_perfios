﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CreditExchange.Syndication.Perfios.Proxy.Request;
using CreditExchange.Syndication.Perfios.Proxy.Response;
using LendFoundry.Foundation.Logging;
using RestSharp;
namespace CreditExchange.Syndication.Perfios.Proxy {
    public class PerfiosProxy : IPerfiosProxy {
        public PerfiosProxy (IPerfiosConfiguration perfiosConfiguration, ILogger logger) {
            if (perfiosConfiguration == null) {
                throw new ArgumentNullException (nameof (perfiosConfiguration));
            }
            if (string.IsNullOrWhiteSpace (perfiosConfiguration.ApiVersion)) {
                throw new ArgumentException ("ApiVersion is require", nameof (perfiosConfiguration.ApiVersion));
            }
            if (string.IsNullOrWhiteSpace (perfiosConfiguration.VendorId)) {
                throw new ArgumentException ("VendorId is require", nameof (perfiosConfiguration.VendorId));
            }
            PerfiosConfiguration = perfiosConfiguration;
            Logger = logger;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }
        private ILogger Logger { get; }
        private IPerfiosConfiguration PerfiosConfiguration { get; }
        public async Task<StartTransactionResponse> StartTransaction (StartTransactionRequest request) {
            if (string.IsNullOrEmpty (PerfiosConfiguration.ServiceUrls.StartTransactionUrl))
                throw new ArgumentException ("Start TransactionURL is require", nameof (PerfiosConfiguration.ServiceUrls.StartTransactionUrl));
            if (string.IsNullOrEmpty (PerfiosConfiguration.PrivateKey))
                throw new ArgumentException ("PrivateKey is require", nameof (PerfiosConfiguration.PrivateKey));
            var baseUri = new Uri (PerfiosConfiguration.ServiceUrls.StartTransactionUrl);
            var uri = PerfiosConfiguration.UseProxy ? new Uri ($"{PerfiosConfiguration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            var payloadxml = XmlSerialization.Serialize<StartTransactionRequest> (request);
            var signature = GenerateSignature.RsaEncrypt (payloadxml, PerfiosConfiguration.PrivateKey);
            var restRequest = new RestRequest (Method.POST);
            var restclient = new RestClient (uri);

            restRequest.AddHeader ("Host", baseUri.Host);
            restRequest.AddParameter ("payload", payloadxml);
            restRequest.AddParameter ("signature", signature);
            var response = await restclient.ExecuteTaskAsync (restRequest);
            if (response.ErrorException != null) {
                Logger.Error ($"[Perfios] Error Exception when StartTransaction method was called. Error:" + response.ErrorException);
                throw new PerfiosException (response.ErrorException.Message);
            }
            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK) {
                Logger.Error ($"[Perfios] Error Exception when StartTransaction method was called. Error:" + response.Content);
                throw new PerfiosException (response.Content);
            }
            Logger.Info ($"[Perfios] StartTransaction method response:", new { response.Content });
            return XmlSerialization.Deserialize<StartTransactionResponse> (response.Content);

        }
        public async Task<TransactionAnalysisReport> RetrieveReportXml (RetrieveReportRequest request) {
            if (string.IsNullOrEmpty (PerfiosConfiguration.ServiceUrls.RetrieveReportsandStatementsUrl))
                throw new ArgumentException ("RetrieveReportsandStatements  is require", nameof (PerfiosConfiguration.ServiceUrls.RetrieveReportsandStatementsUrl));
            if (string.IsNullOrEmpty (PerfiosConfiguration.PrivateKey))
                throw new ArgumentException ("PrivateKey is require", nameof (PerfiosConfiguration.PrivateKey));
            request.ReportType = "xml";
            var baseUri = new Uri (PerfiosConfiguration.ServiceUrls.RetrieveReportsandStatementsUrl);
            var uri = PerfiosConfiguration.UseProxy ? new Uri ($"{PerfiosConfiguration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            var payloadxml = XmlSerialization.Serialize<RetrieveReportRequest> (request);
            var signature = GenerateSignature.RsaEncrypt (payloadxml, PerfiosConfiguration.PrivateKey);
            var restclient = new RestClient (uri);
            var restRequest = new RestRequest (Method.POST);
            restRequest.AddHeader ("Host", baseUri.Host);
            restRequest.AddParameter ("payload", payloadxml);
            restRequest.AddParameter ("signature", signature);
            var response = await restclient.ExecuteTaskAsync (restRequest);

            if (response.ErrorException != null) {
                Logger.Error ($"[Perfios] Error Exception when RetrieveReportXml method was called. Error:" + response.ErrorException);
                return new TransactionAnalysisReport (response.Content, null, payloadxml);
            }

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK) {
                Logger.Error ($"[Perfios] Error Exception when RetrieveReportXml method was called. Error:" + response.Content);
                return new TransactionAnalysisReport (response.Content, null, payloadxml);
            }
            Data report = null;
            try {
                report = XmlSerialization.Deserialize<Data> (response.Content);
            } catch (Exception ex) {

            }

            return new TransactionAnalysisReport (response.Content, report, payloadxml);
        }
        public async Task<byte[]> RetrieveReportPdf (RetrieveReportRequest request) {
            if (string.IsNullOrEmpty (PerfiosConfiguration.ServiceUrls.RetrieveReportsandStatementsUrl))
                throw new ArgumentException ("RetrieveReportsandStatements  is require", nameof (PerfiosConfiguration.ServiceUrls.RetrieveReportsandStatementsUrl));
            if (string.IsNullOrEmpty (PerfiosConfiguration.PrivateKey))
                throw new ArgumentException ("PrivateKey is require", nameof (PerfiosConfiguration.PrivateKey));
            request.ReportType = "pdf";
            var baseUri = new Uri (PerfiosConfiguration.ServiceUrls.RetrieveReportsandStatementsUrl);
            var uri = PerfiosConfiguration.UseProxy ? new Uri ($"{PerfiosConfiguration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            var payloadxml = XmlSerialization.Serialize<RetrieveReportRequest> (request);
            payloadxml = payloadxml.Replace ("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
            payloadxml = payloadxml.Replace ("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
            var signature = GenerateSignature.RsaEncrypt (payloadxml, PerfiosConfiguration.PrivateKey);
            var restclient = new RestClient (uri);
            var restRequest = new RestRequest (Method.POST);

            restRequest.AddHeader ("Host", baseUri.Host);
            restRequest.AddParameter ("payload", payloadxml);
            restRequest.AddParameter ("signature", signature);
            var response = await restclient.ExecuteTaskAsync (restRequest);
            if (response.ErrorException != null) {
                Logger.Error ($"[Perfios] Error Exception when RetrieveReportPdf method was called. Error:" + response.ErrorException);
                throw new PerfiosException ("Report  failed", response.ErrorException);
            }

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK) {
                Logger.Error ($"[Perfios] Error Exception when RetrieveReportPdf method was called. Error:" + response.Content);
                throw new PerfiosException (response.Content);
            }

            return response.RawBytes;

        }
        public async Task<CompleteTransactionResponse> CompleteTransaction (CompleteTransactionRequest request) {
            if (string.IsNullOrEmpty (PerfiosConfiguration.ServiceUrls.CompleteTransactionUrl))
                throw new ArgumentException ("CompleteTransactionURL is require", nameof (PerfiosConfiguration.ServiceUrls.CompleteTransactionUrl));
            if (string.IsNullOrEmpty (PerfiosConfiguration.PrivateKey))
                throw new ArgumentException ("PrivateKey is require", nameof (PerfiosConfiguration.PrivateKey));
            var baseUri = new Uri (PerfiosConfiguration.ServiceUrls.CompleteTransactionUrl);
            var uri = PerfiosConfiguration.UseProxy ? new Uri ($"{PerfiosConfiguration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            var payloadxml = XmlSerialization.Serialize<CompleteTransactionRequest> (request);
            payloadxml = payloadxml.Replace ("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "");
            payloadxml = payloadxml.Replace ("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");

            var signature = GenerateSignature.RsaEncrypt (payloadxml, PerfiosConfiguration.PrivateKey);
            var restclient = new RestClient (uri);
            var restRequest = new RestRequest (Method.POST);

            restRequest.AddHeader ("Host", baseUri.Host);
            restRequest.AddParameter ("payload", payloadxml);
            restRequest.AddParameter ("signature", signature);
            var response = await restclient.ExecuteTaskAsync (restRequest);

            if (response.ErrorException != null) {
                Logger.Error ($"[Perfios] Error Exception when CompleteTransaction method was called. Error:" + response.ErrorException);
                throw new PerfiosException ("CompleteTransaction  failed", response.ErrorException);
            }

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK) {
                Logger.Error ($"[Perfios] Error Exception when CompleteTransaction method was called. Error:" + response.Content);
                throw new PerfiosException (response.Content);
            }
            Logger.Info ($"[Perfios] CompleteTransaction method success", new { response.Content });
            return XmlSerialization.Deserialize<CompleteTransactionResponse> (response.Content);

        }

        public async Task<TransactionStatusResponse> TransactionStatus (TransactionStatusRequest request) {
            if (string.IsNullOrEmpty (PerfiosConfiguration.ServiceUrls.TransactionStatusUrl))
                throw new ArgumentException ("TransactionStatusURL is require", nameof (PerfiosConfiguration.ServiceUrls.TransactionStatusUrl));
            if (string.IsNullOrEmpty (PerfiosConfiguration.PrivateKey))
                throw new ArgumentException ("PrivateKey is require", nameof (PerfiosConfiguration.PrivateKey));
            var baseUri = new Uri (PerfiosConfiguration.ServiceUrls.TransactionStatusUrl);
            var uri = PerfiosConfiguration.UseProxy ? new Uri ($"{PerfiosConfiguration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            var payloadxml = XmlSerialization.Serialize<TransactionStatusRequest> (request);

            var signature = GenerateSignature.RsaEncrypt (payloadxml, PerfiosConfiguration.PrivateKey);
            var restclient = new RestClient (uri);
            var restRequest = new RestRequest (Method.POST);

            restRequest.AddHeader ("Host", baseUri.Host);
            restRequest.AddParameter ("payload", payloadxml);
            restRequest.AddParameter ("signature", signature);
            var response = await restclient.ExecuteTaskAsync (restRequest);

            if (response.ErrorException != null) {
                Logger.Error ($"[Perfios] Error Exception when TransactionStatus method was called. Error:" + response.ErrorException);
                throw new PerfiosException ("TransactionStatus call failed", response.ErrorException);
            }

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK) {
                Logger.Error ($"[Perfios] Error Exception when TransactionStatus method was called. Error:" + response.Content);
                throw new PerfiosException (response.Content);
            }

            Logger.Info ($"[Perfios] TransactionStatus method success", new { response.Content });
            return XmlSerialization.Deserialize<TransactionStatusResponse> (response.Content);

        }
        public async Task<ReviewTransactionResponse> ReviewTransaction (ReviewTransactionRequest request) {
            if (string.IsNullOrEmpty (PerfiosConfiguration.ServiceUrls.RequestReviewofAPerfiosTransactionUrl))
                throw new ArgumentException ("ReviewTransactionURL is require", nameof (PerfiosConfiguration.ServiceUrls.RequestReviewofAPerfiosTransactionUrl));
            if (string.IsNullOrEmpty (PerfiosConfiguration.PrivateKey))
                throw new ArgumentException ("PrivateKey is require", nameof (PerfiosConfiguration.PrivateKey));
            var baseUri = new Uri (PerfiosConfiguration.ServiceUrls.RequestReviewofAPerfiosTransactionUrl);
            var uri = PerfiosConfiguration.UseProxy ? new Uri ($"{PerfiosConfiguration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            var payloadxml = XmlSerialization.Serialize<ReviewTransactionRequest> (request);
            var signature = GenerateSignature.RsaEncrypt (payloadxml, PerfiosConfiguration.PrivateKey);
            var restclient = new RestClient (uri);
            var restRequest = new RestRequest (Method.POST);

            restRequest.AddHeader ("Host", baseUri.Host);
            restRequest.AddParameter ("payload", payloadxml);
            restRequest.AddParameter ("signature", signature);
            var response = await restclient.ExecuteTaskAsync (restRequest);

            if (response.ErrorException != null) {
                Logger.Error ($"[Perfios] Error Exception when ReviewTransaction method was called. Error:" + response.ErrorException);
                throw new PerfiosException ("ReviewTransaction call failed", response.ErrorException);
            }

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK) {
                Logger.Error ($"[Perfios] Error Exception when ReviewTransaction method was called. Error:" + response.Content);
                throw new PerfiosException (response.Content);
            }
            Logger.Info ($"[Perfios] ReviewTransaction method success", new { response.Content });
            return XmlSerialization.Deserialize<ReviewTransactionResponse> (response.Content);

        }
        public async Task<SupportedInstitutions> ListsupportedInstitutions (ListSupportedInstitutionRequest request) {
            if (string.IsNullOrEmpty (PerfiosConfiguration.ServiceUrls.ListofsupportedInstitutionsUrl))
                throw new ArgumentException ("ListofsupportedInstitutionURL is require", nameof (PerfiosConfiguration.ServiceUrls.ListofsupportedInstitutionsUrl));
            if (string.IsNullOrEmpty (PerfiosConfiguration.PrivateKey))
                throw new ArgumentException ("PrivateKey is require", nameof (PerfiosConfiguration.PrivateKey));
            var baseUri = new Uri (PerfiosConfiguration.ServiceUrls.ListofsupportedInstitutionsUrl);
            var uri = PerfiosConfiguration.UseProxy ? new Uri ($"{PerfiosConfiguration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            var payloadxml = XmlSerialization.Serialize<ListSupportedInstitutionRequest> (request);
            var signature = GenerateSignature.RsaEncrypt (payloadxml, PerfiosConfiguration.PrivateKey);
            var restclient = new RestClient (uri);
            var restRequest = new RestRequest (Method.POST);

            restRequest.AddHeader ("Host", baseUri.Host);
            restRequest.AddParameter ("payload", payloadxml);
            restRequest.AddParameter ("signature", signature);
            var response = await restclient.ExecuteTaskAsync (restRequest);

            if (response.ErrorException != null) {
                Logger.Error ($"[Perfios] Error Exception when ListsupportedInstitutions method was called. Error:" + response.ErrorException);
                throw new PerfiosException ($"{response.Content ?? ""}");
            }
            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK) {
                Logger.Error ($"[Perfios] Error Exception when ListsupportedInstitutions method was called. Error:" + response.Content);
                throw new PerfiosException (response.Content);
            }
            Logger.Info ($"[Perfios] ListsupportedInstitutions method success", new { response.Content });

            return XmlSerialization.Deserialize<SupportedInstitutions> (response.Content);

        }
        public async Task<DeletedataResponse> Deletedata (DeletedataRequest request) {
            if (string.IsNullOrEmpty (PerfiosConfiguration.ServiceUrls.DeletedataUrl))
                throw new ArgumentException ("DeletedataURL is require", nameof (PerfiosConfiguration.ServiceUrls.DeletedataUrl));
            if (string.IsNullOrEmpty (PerfiosConfiguration.PrivateKey))
                throw new ArgumentException ("PrivateKey is require", nameof (PerfiosConfiguration.PrivateKey));
            var baseUri = new Uri (PerfiosConfiguration.ServiceUrls.DeletedataUrl);
            var uri = PerfiosConfiguration.UseProxy ? new Uri ($"{PerfiosConfiguration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            var payloadxml = XmlSerialization.Serialize<DeletedataRequest> (request);
            var signature = GenerateSignature.RsaEncrypt (payloadxml, PerfiosConfiguration.PrivateKey);
            var restclient = new RestClient (uri);
            var restRequest = new RestRequest (Method.POST);

            restRequest.AddHeader ("Host", baseUri.Host);
            restRequest.AddParameter ("payload", payloadxml);
            restRequest.AddParameter ("signature", signature);
            var response = await restclient.ExecuteTaskAsync (restRequest);

            if (response.ErrorException != null) {
                Logger.Error ($"[Perfios] Error Exception when Deletedata method was called. Error:" + response.Content);
                throw new PerfiosException ($"{response.Content ?? ""}");
            }

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK) {
                Logger.Error ($"[Perfios] Error Exception when Deletedata method was called. Error:" + response.Content);
                throw new PerfiosException (response.Content);
            }

            Logger.Info ($"[Perfios] Deletedata method success", new { response.Content });
            return XmlSerialization.Deserialize<DeletedataResponse> (response.Content);

        }
        public async Task<string> GenerateNetlinkingForm (StartProcessRequest request) {
            if (string.IsNullOrEmpty (PerfiosConfiguration.ServiceUrls.StartNetLinkingProcessUrl))
                throw new ArgumentException ("Start NetLinking ProcessUrl is require", nameof (PerfiosConfiguration.ServiceUrls.StartNetLinkingProcessUrl));
            if (string.IsNullOrEmpty (PerfiosConfiguration.Destination))
                throw new ArgumentException ("Destination is require", nameof (PerfiosConfiguration.Destination));
            if (string.IsNullOrEmpty (request.EmailId))
                throw new ArgumentException ("Email is require", nameof (request.EmailId));
            if (string.IsNullOrEmpty (PerfiosConfiguration.NetLinkForm))
                throw new ArgumentException ("NetLinkForm is require", nameof (PerfiosConfiguration.NetLinkForm));
            var encryptedEmail = GenerateSignature.PasswordEncrypt (request.EmailId, PerfiosConfiguration.PublicKey);
            request.EmailId = encryptedEmail;
            var payloadxml = XmlSerialization.Serialize<StartProcessRequest> (request);
            var signature = GenerateSignature.RsaEncrypt (payloadxml, PerfiosConfiguration.PrivateKey);
            string netlinkhtml = PerfiosConfiguration.NetLinkForm;

            netlinkhtml = netlinkhtml.Replace ("##payload##", payloadxml);
            netlinkhtml = netlinkhtml.Replace ("##signature##", signature);
            netlinkhtml = netlinkhtml.Replace ("##StartNetLinkingProcessUrl##", PerfiosConfiguration.ServiceUrls.StartNetLinkingProcessUrl);
            return await Task.Run (() => netlinkhtml);
        }

        public async Task<UploadStatementResponse> UploadStatement (UploadStatementRequest request) {

            if (string.IsNullOrEmpty (PerfiosConfiguration.ServiceUrls.UploadStatementUrl))
                throw new ArgumentException ("UploadStatementURL is require", nameof (PerfiosConfiguration.ServiceUrls.UploadStatementUrl));
            var baseUri = new Uri (PerfiosConfiguration.ServiceUrls.UploadStatementUrl);
            var uri = PerfiosConfiguration.UseProxy ? new Uri ($"{PerfiosConfiguration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            var restclient = new RestClient (uri);
            var restRequest = new RestRequest (Method.POST);

            if (!string.IsNullOrWhiteSpace (request.Password)) {
                request.Password = GenerateSignature.PasswordEncrypt (request.Password, PerfiosConfiguration.PublicKey);
            }
            using (var httpClient = new HttpClient ()) {
                using (var clientRequest = new HttpRequestMessage (new HttpMethod ("POST"), PerfiosConfiguration.ServiceUrls.UploadStatementUrl)) {
                    var multipartContent = new MultipartFormDataContent ();
                    multipartContent.Add (new StringContent (request.PerfiosTransactionId), "perfiosTransactionId");
                    multipartContent.Add (new StringContent (request.VendorId), "vendorId");
                    if (!string.IsNullOrWhiteSpace (request.Password)) {
                        multipartContent.Add (new StringContent (request.Password), "password");
                    }
                    multipartContent.Add (new ByteArrayContent (request.File), "file", "file.pdf");
                    clientRequest.Content = multipartContent;
                    var response = await httpClient.SendAsync (clientRequest);

                    using (HttpContent content = response.Content) {
                        var json = content.ReadAsStringAsync ().Result;
                        Console.WriteLine (json);
                        if (response.StatusCode != HttpStatusCode.OK) {
                            throw new PerfiosException (json);
                        }
                        return XmlSerialization.Deserialize<UploadStatementResponse> (json);
                    }
                }
            }

        }
    }
}