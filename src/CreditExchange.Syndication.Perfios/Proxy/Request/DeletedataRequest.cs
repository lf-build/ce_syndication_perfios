﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{

    [XmlRoot(ElementName = "payload")]
    public partial class DeletedataRequest
    {
        [XmlElement(ElementName = "apiVersion")]
        public string ApiVersion { get; set; }
        [XmlElement(ElementName = "perfiosTransactionId")]
        public string PerfiosTransactionId { get; set; }
        [XmlElement(ElementName = "vendorId")]
        public string VendorId { get; set; }
    }
}
