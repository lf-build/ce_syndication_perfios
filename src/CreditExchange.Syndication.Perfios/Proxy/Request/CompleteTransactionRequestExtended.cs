﻿using System;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    public partial class CompleteTransactionRequest
    {
        public CompleteTransactionRequest()
        {

        }
        public CompleteTransactionRequest(IPerfiosConfiguration perfiosConfiguration,string perfiostransactionid)
        {
            if (perfiosConfiguration == null)
                throw new ArgumentNullException(nameof(perfiosConfiguration));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ServiceUrls.CompleteTransactionUrl))
                throw new ArgumentException("CompleteTransactionURL is required", nameof(perfiosConfiguration.ServiceUrls.CompleteTransactionUrl));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(perfiosConfiguration.ApiVersion));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.VendorId))
                throw new ArgumentException("VendorId is required", nameof(perfiosConfiguration.VendorId));
            if (string.IsNullOrWhiteSpace(perfiostransactionid))
                throw new ArgumentException("Perfios TransactionId is required", nameof(perfiostransactionid));
            ApiVersion = perfiosConfiguration.ApiVersion;
            VendorId = perfiosConfiguration.VendorId;
            PerfiosTransactionId = perfiostransactionid;
        }
    }
}
