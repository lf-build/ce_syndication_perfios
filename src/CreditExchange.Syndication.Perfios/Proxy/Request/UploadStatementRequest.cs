﻿namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    public partial class UploadStatementRequest
    {
        public byte[] File { get; set; }
        public string PerfiosTransactionId { get; set; }
        public string VendorId { get; set; }
        public string Password { get; set; } = "";
    }
}
