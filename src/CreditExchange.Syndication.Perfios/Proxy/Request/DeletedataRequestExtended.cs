﻿using CreditExchange.Syndication.Perfios.Request;
using System;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    public partial class DeletedataRequest
    {
        public DeletedataRequest()
        {

        }
        public DeletedataRequest(IPerfiosConfiguration perfiosConfiguration, IDeletedataRequest deletedataRequest)
        {
            if (perfiosConfiguration == null)
                throw new ArgumentNullException(nameof(perfiosConfiguration));
            if (deletedataRequest == null)
                throw new ArgumentNullException(nameof(deletedataRequest));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ServiceUrls.DeletedataUrl))
                throw new ArgumentException("DeletedataURL is required", nameof(perfiosConfiguration.ServiceUrls.DeletedataUrl));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(perfiosConfiguration.ApiVersion));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.VendorId))
                throw new ArgumentException("VendorId is required", nameof(perfiosConfiguration.VendorId));
            if (string.IsNullOrWhiteSpace(deletedataRequest.PerfiosTransactionId))
                throw new ArgumentException("PerfiosTransactionId is required", nameof(deletedataRequest.PerfiosTransactionId));
            ApiVersion = perfiosConfiguration.ApiVersion;
            VendorId = perfiosConfiguration.VendorId;
            PerfiosTransactionId = deletedataRequest.PerfiosTransactionId;
        }
    }
}
