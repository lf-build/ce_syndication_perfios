﻿using CreditExchange.Syndication.Perfios.Request;
using System;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    public partial class ReviewTransactionRequest
    {
        public ReviewTransactionRequest()
        {

        }
        public ReviewTransactionRequest(IPerfiosConfiguration perfiosConfiguration, IReviewTransactionRequest reviewTransactionRequest)
        {
            if (perfiosConfiguration == null)
                throw new ArgumentNullException(nameof(perfiosConfiguration));
            if (reviewTransactionRequest == null)
                throw new ArgumentNullException(nameof(reviewTransactionRequest));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ServiceUrls.RequestReviewofAPerfiosTransactionUrl))
                throw new ArgumentException("Review TransactionURL is required", nameof(perfiosConfiguration.ServiceUrls.RequestReviewofAPerfiosTransactionUrl));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(perfiosConfiguration.ApiVersion));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.VendorId))
                throw new ArgumentException("VendorId is required", nameof(perfiosConfiguration.VendorId));
            if (string.IsNullOrWhiteSpace(reviewTransactionRequest.PerfiosTransactionId))
                throw new ArgumentException("PerfiosTransactionId is required", nameof(reviewTransactionRequest.PerfiosTransactionId));
            ApiVersion = perfiosConfiguration.ApiVersion;
            VendorId = perfiosConfiguration.VendorId;
            PerfiosTransactionId = reviewTransactionRequest.PerfiosTransactionId;
        }
    }
}
