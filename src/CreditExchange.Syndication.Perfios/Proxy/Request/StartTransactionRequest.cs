﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{

    [XmlRoot(ElementName = "payload")]
    public partial class StartTransactionRequest
    {
        [XmlElement(ElementName = "apiVersion")]
        public string ApiVersion { get; set; }
        [XmlElement(ElementName = "vendorId")]
        public string VendorId { get; set; }
        [XmlElement(ElementName = "txnId")]
        public string TxnId { get; set; }
        [XmlElement(ElementName = "institutionId")]
        public string InstitutionId { get; set; }
        [XmlElement(ElementName = "loanAmount")]
        public string LoanAmount { get; set; }
        [XmlElement(ElementName = "loanDuration")]
        public string LoanDuration { get; set; }
        [XmlElement(ElementName = "loanType")]
        public string LoanType { get; set; }
        [XmlElement(ElementName = "yearMonthFrom")]
        public string YearMonthFrom { get; set; }
        [XmlElement(ElementName = "yearMonthTo")]
        public string YearMonthTo { get; set; }
        [XmlElement(ElementName = "acceptancePolicy")]
        public string AcceptancePolicy { get; set; }
    }
}
