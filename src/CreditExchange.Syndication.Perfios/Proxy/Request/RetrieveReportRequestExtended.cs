﻿using System;

namespace CreditExchange.Syndication.Perfios.Proxy.Request
{
    public partial class RetrieveReportRequest
    {
        public RetrieveReportRequest()
        {

        }
        public RetrieveReportRequest(IPerfiosConfiguration perfiosConfiguration, string perfiosTransactionId, string transactionId)
        {
            if (perfiosConfiguration == null)
                throw new ArgumentNullException(nameof(perfiosConfiguration));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ServiceUrls.RetrieveReportsandStatementsUrl))
                throw new ArgumentException("Retrieve Reportsand URL is required", nameof(perfiosConfiguration.ServiceUrls.RetrieveReportsandStatementsUrl));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.ApiVersion))
                throw new ArgumentException("ApiVersion is required", nameof(perfiosConfiguration.ApiVersion));
            if (string.IsNullOrWhiteSpace(perfiosConfiguration.VendorId))
                throw new ArgumentException("VendorId is required", nameof(perfiosConfiguration.VendorId));
            if (string.IsNullOrWhiteSpace(perfiosTransactionId))
                throw new ArgumentException("PerfiosTransactionId is required", nameof(perfiosTransactionId));
            if (string.IsNullOrWhiteSpace(transactionId))
                throw new ArgumentException("TransactionId is required", nameof(transactionId));
            PerfiosTransactionId = perfiosTransactionId;
            TxnId = transactionId;
            VendorId = perfiosConfiguration.VendorId;
            ApiVersion = perfiosConfiguration.ApiVersion;
           
        }
    }
}
