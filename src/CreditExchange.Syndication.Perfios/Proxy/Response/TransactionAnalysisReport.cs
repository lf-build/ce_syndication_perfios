﻿namespace CreditExchange.Syndication.Perfios.Proxy.Response
{
    public class TransactionAnalysisReport
    {
        public TransactionAnalysisReport()
        {

        }
        public TransactionAnalysisReport(string responseXMLStr,Data report,string requestXMLStr)
        {
            ResponseXMLStr = responseXMLStr;
            RequestXMLStr = requestXMLStr;
            Report = report;
        }
        public string ResponseXMLStr { get; set; }
        
        public string RequestXMLStr { get; set; }
        public Data Report { get; set; }
    }
}
