﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Response
{

    [XmlRoot(ElementName = "Status")]
    public class TransactionStatusResponse
    {
        [XmlElement(ElementName = "Part")]
        public List<Part> Part { get; set; }
        [XmlAttribute(AttributeName = "txnId")]
        public string TxnId { get; set; }
        [XmlAttribute(AttributeName = "parts")]
        public string Parts { get; set; }
        [XmlAttribute(AttributeName = "processing")]
        public string Processing { get; set; }
        [XmlAttribute(AttributeName = "files")]
        public string Files { get; set; }
    }
    [XmlRoot(ElementName = "Part")]
    public class Part
    {
        [XmlAttribute(AttributeName = "perfiosTransactionId")]
        public string PerfiosTransactionId { get; set; }
        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
        [XmlAttribute(AttributeName = "errorCode")]
        public string ErrorCode { get; set; }
        [XmlAttribute(AttributeName = "reason")]
        public string Reason { get; set; }
    }
}
