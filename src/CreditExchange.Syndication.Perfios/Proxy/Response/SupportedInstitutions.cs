﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Response
{
   
    public class Institution
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "institutionType")]
        public string InstitutionType { get; set; }
        [XmlElement(ElementName = "addressAvailable")]
        public string AddressAvailable { get; set; }
    }
    [XmlRoot(ElementName = "Institutions")]
    public class SupportedInstitutions
    {
        [XmlElement(ElementName = "Institution")]
        public List<Institution> Institution { get; set; }
    }
}
