﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Response
{
    
    [XmlRoot(ElementName = "Success")]
    public class UploadStatementResponse
    {
        [XmlElement(ElementName = "statementId")]
        public string StatementId { get; set; }
    }
}
