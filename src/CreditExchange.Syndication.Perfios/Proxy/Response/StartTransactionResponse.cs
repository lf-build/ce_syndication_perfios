﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Perfios.Proxy.Response
{
   
    [XmlRoot(ElementName = "Success")]
    public class StartTransactionResponse
    {
        [XmlElement(ElementName = "perfiosTransactionId")]
        public string perfiosTransactionId { get; set; }
    }
}
